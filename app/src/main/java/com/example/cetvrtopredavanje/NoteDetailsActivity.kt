package com.example.cetvrtopredavanje

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_note_details.*
import java.text.SimpleDateFormat
import java.util.*


class NoteDetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_note_details)

        if(intent.getIntExtra("novo", 0).equals(0)){
            val position = intent.getIntExtra("pozicija", -1)
            val note = NotesList.notesList.get(position)
            notesTitleText.setText(note.noteTitle)
            noteDescription.setText(note.noteDescription)
            Toast.makeText(this,"EDITED",Toast.LENGTH_SHORT)
        }

        buttonSave.setOnClickListener {
            var note = Note()
            val simpleDateFormat = SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss")
            val currentDate = simpleDateFormat.format((Date()))
            if(intent.getIntExtra("novo",1).equals(1)){
                note.noteDescription = noteDescription.text.toString()
                note.noteTitle = notesTitleText.text.toString()
                note.noteDate = currentDate
                NotesList.notesList.add(note)
                Toast.makeText(this,"NEW ADDED",Toast.LENGTH_SHORT)
                finish()
            }
            else{
                val position = intent.getIntExtra("pozicija", -1)
                val note = NotesList.notesList.get(position)
                note.noteTitle = notesTitleText.text.toString()
                note.noteDescription = noteDescription.text.toString()
                note.noteDate = currentDate
                finish()
            }
        }

    }
}
