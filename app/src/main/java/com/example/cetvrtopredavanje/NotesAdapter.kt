package com.example.cetvrtopredavanje

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.RelativeLayout
import android.widget.TextView
import java.text.SimpleDateFormat
import java.util.*


class NotesAdapter: RecyclerView.Adapter<NotesAdapter.ViewHolder> (){

    var EDIT: Int = 2

    class ViewHolder(itemView: View, context2: Context):RecyclerView.ViewHolder(itemView){
        var notesListTextView: TextView? = null
        var notesDate: TextView? = null
        var deleteButton: ImageButton? = null
        var relativeLayoutAllNotes: RelativeLayout? = null
        var context: Context? = null

        init{
            notesListTextView = itemView.findViewById(R.id.textView)
            notesDate = itemView.findViewById(R.id.notesDate)
            deleteButton = itemView.findViewById(R.id.buttonDelete)
            relativeLayoutAllNotes = itemView.findViewById(R.id.noteTitleTextView)
            context = context2
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val notesListElement = inflater.inflate(R.layout.note_title, parent, false)
        return ViewHolder(notesListElement,context)
    }

    override fun getItemCount(): Int {
        return NotesList.notesList.size
    }

    override fun onBindViewHolder(parent: ViewHolder, position: Int) {
        parent.notesListTextView?.text = NotesList.notesList[position].noteTitle

        parent.notesDate?.text = NotesList.notesList[position].noteDate

        parent.deleteButton?.setOnClickListener {
            NotesList.notesList.removeAt(position)
            notifyDataSetChanged()
            print("Deleted note on position"+position)

        }
        parent.relativeLayoutAllNotes?.setOnClickListener {

            var intent: Intent = Intent(parent.context, NoteDetailsActivity::class.java)
            intent.putExtra("novo",0)
            intent.putExtra("pozicija",position)
            (parent.context as Activity).startActivityForResult(intent, EDIT)

        }
    }
}