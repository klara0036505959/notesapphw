package com.example.cetvrtopredavanje

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var notesAdapter: NotesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        listOfNotesRecycleView.layoutManager = LinearLayoutManager(this)
        notesAdapter = NotesAdapter()
        listOfNotesRecycleView.adapter = notesAdapter

        floatingActionButton.setOnClickListener{
            val startNoteDetailsActivity = Intent(this, NoteDetailsActivity::class.java)
            startNoteDetailsActivity.putExtra("novo",1)
            startActivity(startNoteDetailsActivity)
        }

    }

    override fun onResume() {
        super.onResume()
        notesAdapter.notifyDataSetChanged()
    }
}
